;; -*-scheme-*-

(define (script-fu-cartoon image
                           drawable
                           shadow-level
                           edge-size
                           blur-radius)

  (let* ((work-img 0)
	 (cartoon-value-layer 0)
	 (cartoon-saturation-layer 0)
	 (cartoon-hue-layer 0)
	 (cartoon-img 0)
	 )

    (set! work-img (car (plug-in-decompose 1 image drawable "HSV" 1)))
    (set! cartoon-hue-layer (aref
			       (car
				(cdr  (gimp-image-get-layers work-img)
				      )
				)
			       0)
	  )
    (set! cartoon-saturation-layer (aref
				    (car
				     (cdr  (gimp-image-get-layers work-img)
					   )
				     )
				    1)
	  )
    (set! cartoon-value-layer (aref
			     (car
			      (cdr  (gimp-image-get-layers work-img)
				    )
			      )
			     2)
	  )
    (if (> shadow-level 0)
	(begin
	  (gimp-by-color-select cartoon-value-layer '(0 0 0) shadow-level 2 1 1 3 0)
	  (gimp-selection-invert work-img)
	  )
	)
    (plug-in-edge 1 work-img cartoon-value-layer edge-size 1 0)
    (plug-in-sel-gauss 1 work-img cartoon-value-layer blur-radius 50)
    (gimp-invert cartoon-value-layer)

    (set! cartoon-img (car (plug-in-drawable-compose 1 0
						     cartoon-hue-layer
						     cartoon-saturation-layer
						     cartoon-value-layer
						     0
						     "HSV"))
	  )
    (gimp-image-delete work-img)
    (gimp-display-new cartoon-img)
    (gimp-displays-flush)
    )
  )

(script-fu-register "script-fu-cartoon"
		    _"<Image>/Filters/Artistic/Edgy car_toon ..."
		    "Produce a cartoonish look, with an edge detect."
		    "Roland Sieker"
		    "Roland Sieker, 2007–2020"
		    "2007, 2020"
		    "RGB*"
		    SF-IMAGE    "Image"    0
		    SF-DRAWABLE "Drawable" 0
                    SF-ADJUSTMENT _"Shadow level"         '(40 0 255 1 10 0 0)
                    SF-ADJUSTMENT _"Edge detection size"  '(5 0.1 50 1 0.1 1 0)
                    SF-ADJUSTMENT _"Blur radius"          '(10 0.1 100 1 0.1 1 0))

;(script-fu-menu-register "script-fu-cartoon"
;			 "<Image>/Script-Fu/Alchemy")
