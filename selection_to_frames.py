#!/usr/bin/python
# -*- mode: Python ; coding: utf-8 -*-

# © 2020 Roland Sieker <ospalh@gmail.com>
# This plug-in is based on foggify,
# Copyright (C) 1997  James Henstridge <james@daa.com.au>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from gimpfu import *

gettext.install("gimp20-python", gimp.locale_directory, unicode=True)

def mark_frames(image, layer, name, color, opacity, border_width, border_fuzz):
    gimp.context_push()
    image.undo_group_start()
    if image.base_type == RGB:
        base_type = RGBA_IMAGE
    else:
        base_type = GRAYA_IMAGE
    frames = gimp.Layer(
        image, name, image.width, image.height, base_type, opacity,
        NORMAL_MODE)
    frames.fill(FILL_TRANSPARENT)
    image.insert_layer(frames)
    gimp.set_foreground(color)
    pdb.gimp_selection_border(image, border_width)
    pdb.gimp_selection_feather(image, border_fuzz)
    pdb.gimp_edit_fill(frames, FILL_FOREGROUND)
    image.undo_group_end()
    gimp.context_pop()

register(
    "python-fu-mark-frames",
    N_("Make frames out of selection"),
    "Adds a layer with frames following the borders of the selection.",
    "Roland Sieker",
    "Roland Sieker",
    "1999,2007,2020",
    N_("_Frames..."),
    "RGB*, GRAY*",
    [
        (PF_IMAGE, "image",       "Input image", None),
        (PF_DRAWABLE, "drawable", "Input drawable", None),
        (PF_STRING, "name",       _("_layer name"), _("Frames")),
        (PF_COLOUR, "color",     _("frame _color"),  (193, 34, 34)),
        (PF_SLIDER, "opacity",    _("Op_acity"),    70, (0, 100, 1)),
        (PF_SLIDER, "border_width",    _("Border _width"),    5, (1, 30, 1)),
        (PF_SLIDER, "border_fuzz",    _("Feeathering width"),    5, (1, 30, 1)),
    ],
    [],
    mark_frames,
    menu="<Image>/Filters/Web",
    domain=("gimp20-python", gimp.locale_directory)
    )

# print 'selectiontoframe'
main()
